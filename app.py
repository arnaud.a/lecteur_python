from flask import Flask
from flask import render_template
from forms import UserForm
from models import *
import requests

import click

app = Flask(__name__)
app.secret_key = 'kappa' #Don't use it .. !

@app.route('/')
def hello_world():
    return render_template('home.html')

@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/listeFlux')
def listeFlux():
    return render_template('listeFlux.html')

@app.route('/afficherFlux/<fluxName>')
def afficherFlux(fluxName=None):
    return render_template('afficherFlux.html', fluxContent=requests.get(getFluxContentByFluxName(fluxName)).content)


@app.route('/everything')
def everything():
    users = User.select()
    return render_template('hello.html',users=users)

@app.route('/user/create')
def user_register():
    form = UserForm()
    return render_template('/Users/form.html', form=form)

@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')


@app.cli.command()
def fakedata():
    from faker import Faker
    from slugify import slugify
    fake = Faker()
    for pk in range(0, 42):
        name = fake.name()
        User.create(name=name,
                         slug=slugify(name, to_lower=True),
                         email = "test@test.fr",
                         password=fake.catch_phrase(),
                         inscription_date=fake.date())
    
    for j in range(0,10):
        name = fake.name()
        Stream.create(name=name,
                        url = "https://test.fr")



def getFluxContentByFluxName(fluxName):
    if fluxName == 'starcraft':
        return 'https://starcraft2.judgehype.com/nouvelles.xml'
        
    if fluxName == 'raspBerryPi':
        return 'https://raspberrypi.org/blog/feed'
