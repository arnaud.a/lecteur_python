from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms .fields import PasswordField, BooleanField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length


class UserForm(FlaskForm):
    name = StringField('name', validators=[
                       DataRequired(), Length(min=3, max=20)])
    mail = StringField('email', validators=[
                       DataRequired(), Length(min=3)])
    password = PasswordField('password', validators=[DataRequired(), Length(min=8)])
    accept_rules = BooleanField('I accept the site rules', validators=[DataRequired()])



class StreamForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(), Length(min=3)])