from peewee import *
import datetime

database = SqliteDatabase("lecteur_database.sqlite3")

class BaseModel(Model):

    class Meta:
        database = database


class User(BaseModel):
    name = CharField()
    slug = CharField()
    email = TextField()
    password = TextField()  
    inscription_date= DateTimeField(default=datetime.datetime.now)

class Stream(BaseModel):
    name = CharField()
    url = TextField()

class Connection(BaseModel):
    user = ForeignKeyField(User, backref="connection")
    stream = ForeignKeyField(Stream, backref="connection")


def create_tables():
    with database:
        database.create_tables([User, ])
        database.create_tables([Stream,])
        database.create_tables([Connection,])


def drop_tables():
    with database:
        database.drop_tables([User, ])
        database.drop_tables([Stream,])
        database.drop_tables([Connection,])  